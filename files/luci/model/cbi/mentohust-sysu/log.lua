local fs = require "nixio.fs"

local f = SimpleForm("mentohust-sysu",
	translate("MentoHUST(SYSU) LOG"),
	translate("Log file:/tmp/mentohust-sysu.log"))

local o = f:field(Value, "mentohust-sysu_log")

o.template = "cbi/tvalue"
o.rows = 32

function o.cfgvalue(self, section)
	return fs.readfile("/tmp/mentohust-sysu.log")
end

function o.write(self, section, value)
	require("luci.sys").call('cat /dev/null > /tmp/mentohust-sysu.log 2>/dev/null')
end

f.submit = translate("Clear log")
f.reset = false

return f