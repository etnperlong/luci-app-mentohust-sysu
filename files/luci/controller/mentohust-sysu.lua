module("luci.controller.mentohust-sysu", package.seeall)

function index()
    if not nixio.fs.access("/etc/config/mentohust-sysu") then
        return
    end
    if luci.sys.call("command -v mentohust-sysu >/dev/null") ~= 0 then
        return
    end
    entry({"admin", "services", "mentohust-sysu"},
        alias("admin", "services", "mentohust-sysu", "general"),
        _("MentoHUST(SYSU)"), 10).dependent = true

    entry({"admin", "services", "mentohust-sysu", "general"}, cbi("mentohust-sysu/general"), _("MentoHUST(SYSU) Settings"), 10).leaf = true
    entry({"admin", "services", "mentohust-sysu", "log"}, cbi("mentohust-sysu/log"), _("MentoHUST(SYSU) LOG"), 20).leaf = true
end
