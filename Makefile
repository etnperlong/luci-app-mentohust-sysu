include $(TOPDIR)/rules.mk

PKG_NAME:=luci-app-mentohust-sysu
PKG_VERSION=4.2
PKG_RELEASE:=7

PKG_BUILD_DIR:=$(BUILD_DIR)/$(PKG_NAME)

include $(INCLUDE_DIR)/package.mk

define Package/luci-app-mentohust-sysu
	SECTION:=luci
	CATEGORY:=LuCI
	SUBMENU:=3. Applications
	TITLE:=mentohust-sysu 802.1X Client for LuCI
	PKGARCH:=all
endef

define Package/luci-app-mentohust-sysu/description
	This package contains LuCI configuration pages for 8021xclient.
endef

define Package/luci-app-mentohust-sysu/conffiles
/etc/config/mentohust-sysu
endef

define Build/Prepare
	$(foreach po,$(wildcard ${CURDIR}/files/luci/i18n/*.po), \
		po2lmo $(po) $(PKG_BUILD_DIR)/$(patsubst %.po,%.lmo,$(notdir $(po)));)
endef

define Build/Configure
endef

define Build/Compile
endef

define Package/luci-app-mentohust-sysu/install
	$(INSTALL_DIR) $(1)/etc/config
	$(INSTALL_DIR) $(1)/etc/init.d
	$(INSTALL_DIR) $(1)/usr/lib/lua/luci/model/cbi/mentohust-sysu
	$(INSTALL_DIR) $(1)/usr/lib/lua/luci/controller
	$(INSTALL_DIR) $(1)/usr/lib/lua/luci/i18n
	$(INSTALL_DATA) $(PKG_BUILD_DIR)/mentohust-sysu.*.lmo $(1)/usr/lib/lua/luci/i18n/
	$(INSTALL_CONF) ./files/root/etc/config/mentohust-sysu $(1)/etc/config/mentohust-sysu
	$(INSTALL_BIN) ./files/root/etc/init.d/mentohust-sysu $(1)/etc/init.d/mentohust-sysu
	$(INSTALL_DATA) ./files/luci/model/cbi/mentohust-sysu/general.lua $(1)/usr/lib/lua/luci/model/cbi/mentohust-sysu/general.lua
	$(INSTALL_DATA) ./files/luci/model/cbi/mentohust-sysu/log.lua $(1)/usr/lib/lua/luci/model/cbi/mentohust-sysu/log.lua
	$(INSTALL_DATA) ./files/luci/controller/mentohust-sysu.lua $(1)/usr/lib/lua/luci/controller/mentohust-sysu.lua
endef

$(eval $(call BuildPackage,luci-app-mentohust-sysu))
